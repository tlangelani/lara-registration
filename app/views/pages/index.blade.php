@extends('layout')

@section('content')
    <h1>Laravel App</h1>
    <h3>
        {{ Auth::check() ? "Welcome, " . Auth::user()->username : "Why don't you sign up?" }}
    </h3>
    <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam dolorum eum expedita impedit modi necessitatibus possimus, qui quis quisquam ratione saepe voluptates. Doloribus dolorum ex explicabo inventore nesciunt tenetur.
    </p>
@stop