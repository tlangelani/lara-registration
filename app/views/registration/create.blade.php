@extends('layout')

@section('content')

    <h1>Register</h1>

    {{ Form::open(['route' => 'registration.store'])  }}

    <div class="form-group">
        {{ Form::label('username', 'Username') }}
        {{ Form::text('username', null, [ 'class' => 'form-control', 'required' => 'required' ]) }}
        {{ errors_for('username', $errors) }}
    </div>

    <div class="form-group">
        {{ Form::label('email', 'Email') }}
        {{ Form::text('email', null, [ 'class' => 'form-control', 'required' => 'required' ]) }}
        {{ errors_for('email', $errors) }}
    </div>

    <div class="form-group">
        {{ Form::label('password', 'Password') }}
        {{ Form::password('password', [ 'class' => 'form-control', 'required' => 'required' ]) }}
        {{ errors_for('password', $errors) }}
    </div>

    <div class="form-group">
        {{ Form::label('password_confirmation', 'Confirm Password') }}
        {{ Form::password('password_confirmation', [ 'class' => 'form-control', 'required' => 'required' ]) }}
    </div>

    <div class="form-group">
        {{ Form::submit('Create Account', ['class' => 'btn btn-primary']) }}
    </div>

    {{ Form::close() }}
@stop
